#pragma once

#include <ctrl.h>
#include <touch.h>
#include <vectormath.h>
#include "Singleton.hpp"

#define g_pInputManager InputManager::Get()

using namespace sce::Vectormath::Simd::Aos;

class InputManager : public Singleton<InputManager>
{
	private:
		SceCtrlData ctrlData;
		SceTouchData tdFront, tdBack;
		SceTouchPanelInfo piFront, piBack;
		Vector2 oldTouchPositionBack, oldTouchPositionFront;

	public:
		InputManager();
		~InputManager();

		SceCtrlData getData();
		Vector2 getFrontPanelTouchPosition();
		Vector2 getBackPanelTouchPositionDelta();
		Vector2 getFrontPanelTouchPositionDelta();
		SceTouchPanelInfo getPanelInfoBack();
		SceTouchPanelInfo getPanelInfoFront();

		Vector2 touchPositionToRenderPosition(Vector2 touchPosition);

		void init();
		void update();

		bool isLButtonPressed();
		bool isRButtonPressed();
		bool isUpButtonPressed();
		bool isDownButtonPressed();
		bool isRightButtonPressed();
		bool isLeftButtonPressed();
		bool isTriangleButtonPressed();
		bool isCrossButtonPressed();
		bool isStartButtonPressed();
};

