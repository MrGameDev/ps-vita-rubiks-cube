#include "CubeHandler.hpp"

CubeHandler::CubeHandler()
{
	rotate = 0;
	currentSide = top;
	receiveInput = true;

	microCubes = g_pCubeRenderer->getMicroCubes();
}

CubeHandler::~CubeHandler()
{
}

void CubeHandler::update()
{
	if (receiveInput)
	{
		handleInput();
		handleFrontTouchInput();
	}
	
	if (rotate != 0)
	{
		if (g_pCubeRenderer->getRotationAlpha() < 90.f && g_pCubeRenderer->getRotationAlpha() > -90.f)
		{
			receiveInput = false;

			//rotate cubeRenderer until rotation animation has finished
			switch (currentSide)
			{
				case top:
					g_pCubeRenderer->rotateTopSide(rotate); //also increases rotationAlpha
					break;
				case front:
					g_pCubeRenderer->rotateFrontSide(rotate);
					break;
				case bottom:
					g_pCubeRenderer->rotateBottomSide(-rotate);
					break;
				case back:
					g_pCubeRenderer->rotateBackSide(-rotate);
					break;
				case left:
					g_pCubeRenderer->rotateLeftSide(rotate);
					break;
				case right:
					g_pCubeRenderer->rotateRightSide(-rotate);
					break;
			}
		}
		else
		{
			//rotate cube logic
			switch (currentSide)
			{
				case top:
					if (rotate == 1)
						g_pCubeLogic->rotateTopSideClockwise();
					else
						g_pCubeLogic->rotateTopSideCounterClockwise();
					break;
				case front:
					if (rotate == 1)
						g_pCubeLogic->rotateFrontSideClockwise();
					else
						g_pCubeLogic->rotateFrontSideCounterClockwise();
					break;
				case bottom:
					if (rotate == 1)
						g_pCubeLogic->rotateBottomSideClockwise();
					else
						g_pCubeLogic->rotateBottomSideCounterClockwise();
					break;
				case back:
					if (rotate == 1)
						g_pCubeLogic->rotateBackSideClockwise();
					else
						g_pCubeLogic->rotateBackSideCounterClockwise();
					break;
				case left:
					if (rotate == 1)
						g_pCubeLogic->rotateLeftSideClockwise();
					else
						g_pCubeLogic->rotateLeftSideCounterClockwise();
					break;
				case right:
					if (rotate == 1)
						g_pCubeLogic->rotateRightSideClockwise();
					else
						g_pCubeLogic->rotateRightSideCounterClockwise();
					break;
			}

			//reset rotate and rotationAlpha
			g_pCubeRenderer->setRotationAlpha(0.f);
			rotate = 0;
			receiveInput = true;
		}
	}
}

void CubeHandler::handleInput()
{
	//update rotation direction
	if (g_pInputManager->isRButtonPressed())
		rotate = 1; //clockwise
	else if (g_pInputManager->isLButtonPressed())
		rotate = -1; //counter clockwise

	updateCurrentSide();
}

void CubeHandler::updateCurrentSide()
{
	//check input to update selected row
	if (g_pInputManager->isUpButtonPressed())
		currentSide = top;
	else if (g_pInputManager->isDownButtonPressed())
		currentSide = bottom;
	else if (g_pInputManager->isLeftButtonPressed())
		currentSide = left;
	else if (g_pInputManager->isRightButtonPressed())
		currentSide = right;
	else if (g_pInputManager->isTriangleButtonPressed())
		currentSide = back;
	else if (g_pInputManager->isCrossButtonPressed())
		currentSide = front;
}

//return x,y coordinates of touchposition on cube AND the cube side
//Vector3(side, x, y)
Vector3 CubeHandler::getTouchedCubeSide(Vector2 touchPos)
{
	if (touchPos.getX() != -1)
	{
		touchVec = g_pInputManager->touchPositionToRenderPosition(touchPos);
		float touchX = touchVec.getX();
		float touchY = -touchVec.getY();
		Matrix3 rotMat = g_pCubeGraphics->rotMatrix.getUpper3x3();
		Matrix4 transMat = g_pCubeGraphics->mvpMatrix;
		Matrix4 invTransMat = inverse(transMat);
		Vector4 nearV(touchX, touchY, 0.1f, 1.f);
		Vector4 farV(touchX, touchY, 0.9f, 1.f);

		nearV = invTransMat * nearV;
		farV = invTransMat * farV;

		Vector3 p1 = nearV.getXYZ() / nearV.getW();
		Vector3 p2 = farV.getXYZ() / farV.getW();

		Vector3 direction = normalize(p2 - p1);

		for (int i = 0; i < 6; i++)
		{
			Vector3 s, center, normal, u, v;

			//top
			if (i == top)
			{
				center = Vector3(0.f, -3.f, 0.f);
				normal = Vector3(0.f, -1.f, 0.f);
				u = Vector3(1.f, 0.f, 0.f);
				v = Vector3(0.f, 0.f, 1.f);
			}
			//front
			else if (i == front)
			{
				center = Vector3(0.f, 0.f, -3.f);
				normal = Vector3(0.f, 0.f, -1.f);
				u = Vector3(1.f, 0.f, 0.f);
				v = Vector3(0.f, -1.f, 0.f); 
			}
			//bottom
			else if (i == bottom)
			{
				center = Vector3(0.f, 3.f, 0.f);
				normal = Vector3(0.f, 1.f, 0.f);
				u = Vector3(-1.f, 0.f, 0.f);
				v = Vector3(0.f, 0.f, 1.f);
			}
			//back
			else if (i == back)
			{
				center = Vector3(0.f, 0.f, 3.f);
				normal = Vector3(0.f, 0.f, 1.f);
				u = Vector3(1.f, 0.f, 0.f);
				v = Vector3(0.f, 1.f, 0.f);
			}
			//left
			else if (i == left)
			{
				center = Vector3(-3.f, 0.f, 0.f);
				normal = Vector3(-1.f, 0.f, 0.f);
				u = Vector3(0.f, 0.f, 1.f);
				v = Vector3(0.f, -1.f, 0.f);
			}
			//right
			else if (i == right)
			{
				center = Vector3(3.f, 0.f, 0.f);
				normal = Vector3(1.f, 0.f, 0.f);
				u = Vector3(0.f, 0.f, 1.f);
				v = Vector3(0.f, -1.f, 0.f);
			}

			float dotDN = dot(direction, normal);

			if (dotDN > 0)
				continue; //if side is not visible

			float alpha = dot(center - p1, normal) / dot(direction, normal);
			s = p1 + alpha * direction;

			float x = dot(u, s - center);
			float y = dot(v, s - center);

			float epsilon = 3.0001f;
			if (x > -epsilon && x < epsilon && y > -epsilon && y < epsilon)
			{
				currentSideH = Vector4(u, 1.f);
				currentSideK = Vector4(v, 1.f);
				return Vector3(i, x, y);
			}
		}
	}

	return Vector3(-1, -4, -4);
}

//determines how a touched side should be rotated
void CubeHandler::handleFrontTouchInput()
{
	Matrix4 transMat = g_pCubeGraphics->mvpMatrix;
	Vector3 touchSideData = getTouchedCubeSide(g_pInputManager->getFrontPanelTouchPosition());

	Vector2 swipe(0.f, 0.f);

	if (touchSideData.getX() != -1)
	{
		int tmpSide = touchSideData.getX();
		float sideX = touchSideData.getY();
		float sideY = touchSideData.getZ();

		currentSideH = transMat * currentSideH;
		currentSideK = transMat * currentSideK;

		swipe += g_pInputManager->getFrontPanelTouchPositionDelta();

		if (length(swipe) < 0.01f)
			return;

		float swipeX = sideX + swipe.getX();
		float swipeY = sideY + swipe.getY();

		Vector3 curH = currentSideH.getXYZ() / currentSideH.getW();
		Vector3 curK = currentSideK.getXYZ() / currentSideK.getW();
		Vector2 h, k;
		Vector2 z = normalize(Vector2(swipeX - sideX, swipeY - sideY));

		h = curH.getXY();
		k = curK.getXY();

		h = normalize(h);
		k = normalize(k);

		if (abs(dot(h, z)) < abs(dot(k, z)))
		{
			//rotate around u
			//choose side to rotate
			if (tmpSide == top || tmpSide == front || tmpSide == back)
			{
				if (sideX < -1)
					currentSide = left;
				else if (sideX > 1)
					currentSide = right;
			}
			else if (tmpSide == bottom)
			{
				if (sideX < -1)
					currentSide = right;
				else if (sideX > 1)
					currentSide = left;
			}
			else if (tmpSide == left)
			{
				if (sideX < -1)
					currentSide = front;
				else if (sideX > 1)
					currentSide = back;
			}
			else if (tmpSide == right)
			{
				if (sideX < -1)
					currentSide = front;
				else if (sideX > 1)
					currentSide = back;
			}

			//rotation direction
			switch (tmpSide)
			{
			case top:
				if (currentSide == right)
					rotate = dot(z, k) > 0 ? -1 : 1;
				else
					rotate = dot(z, k) > 0 ? 1 : -1;
				break;
			case front:
				if (currentSide == right)
					rotate = dot(z, k) > 0 ? -1 : 1;
				else
					rotate = dot(z, k) > 0 ? 1 : -1;
				break;
			case bottom:
				if (currentSide == right)
					rotate = dot(z, k) > 0 ? 1 : -1;
				else
					rotate = dot(z, k) > 0 ? -1 : 1;
				break;
			case back:
				if (currentSide == right)
					rotate = dot(z, k) > 0 ? -1 : 1;
				else
					rotate = dot(z, k) > 0 ? 1 : -1;
				break;
			case left:
				if (currentSide == front)
					rotate = dot(z, k) > 0 ? -1 : 1;
				else
					rotate = dot(z, k) > 0 ? 1 : -1;
				break;
			case right:
				if (currentSide == front)
					rotate = dot(z, k) > 0 ? 1 : -1;
				else
					rotate = dot(z, k) > 0 ? -1 : 1;
				break;
			}
		}
		else if (abs(dot(h, z)) > abs(dot(k, z)))
		{
			//rotate around v
			//choose side to rotate
			if (tmpSide == top)
			{
				if (sideY < -1)
					currentSide = front;
				else if (sideY > 1)
					currentSide = back;
			}
			else if (tmpSide == bottom)
			{
				if (sideY < -1)
					currentSide = front;
				else if (sideY > 1)
					currentSide = back;
			}
			else if (tmpSide == back)
			{
				if (sideY < -1)
					currentSide = top;
				else if (sideY > 1)
					currentSide = bottom;
			}
			else if (tmpSide == front || tmpSide == left || tmpSide == right)
			{
				if (sideY < -1)
					currentSide = bottom;
				else if (sideY > 1)
					currentSide = top;
			}

			//rotation direction
			switch (tmpSide)
			{
			case top:
				if (currentSide == back)
					rotate = dot(z, h) > 0 ? -1 : 1;
				else
					rotate = dot(z, h) > 0 ? 1 : -1;
				break;
			case front:
				if (currentSide == top)
					rotate = dot(z, h) > 0 ? -1 : 1;
				else
					rotate = dot(z, h) > 0 ? 1 : -1;
				break;
			case bottom:
				if (currentSide == back)
					rotate = dot(z, h) > 0 ? -1 : 1;
				else
					rotate = dot(z, h) > 0 ? 1 : -1;
				break;
			case back:
				if (currentSide == top)
					rotate = dot(z, h) > 0 ? 1 : -1;
				else
					rotate = dot(z, h) > 0 ? -1 : 1;
				break;
			case left:
				if (currentSide == top)
					rotate = dot(z, h) > 0 ? 1 : -1;
				else
					rotate = dot(z, h) > 0 ? -1 : 1;
				break;
			case right:
				if (currentSide == top)
					rotate = dot(z, h) > 0 ? -1 : 1;
				else
					rotate = dot(z, h) > 0 ? 1 : -1;
				break;
			}
		}
	}
}