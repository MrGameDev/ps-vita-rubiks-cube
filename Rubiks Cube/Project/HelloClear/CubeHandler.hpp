#pragma once
#include "CubeLogic.hpp"
#include "CubeRenderer.hpp"
#include "InputManager.hpp"

class CubeHandler
{
	private:
		enum CubeSides { top, front, bottom, back, left, right };
		
		int  rotate; //1 = rotate clockwise, -1 =  rotate counter clockwise, 0 = do not rotate
		int currentSide;
		bool receiveInput;
		MicroCube *microCubes;
		Vector4 currentSideH, currentSideK;
		Vector2 touchVec;

	
	public:
		CubeHandler();
		~CubeHandler();

		void update();
		void handleInput();
		void updateCurrentSide();
		
		Vector3 getTouchedCubeSide(Vector2 touchPos);
		void handleFrontTouchInput();
};

