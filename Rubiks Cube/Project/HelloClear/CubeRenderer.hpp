#pragma once
#include <vectormath.h>
#include "MicroCube.hpp"
#include "CubeLogic.hpp"
#include "Singleton.hpp"

#define RAD (3.1415f / 180.f)
#define g_pCubeRenderer CubeRenderer::Get()

class CubeRenderer : public Singleton<CubeRenderer>
{
	private:
		float tileSize; //specifies the width of each micro cube
		MicroCube microCubes[27];
		float rotationAlpha;  //will be increased by alpha value on each rotation
		float rotationSpeed;

		enum CubeRows {top, center, bottom};

	public:
		CubeRenderer();
		~CubeRenderer();

		void createWholeCube();

		void init();
		void renderWholeCube();

		void rotateTopSide(int sign); //sign 1 = clockwise, sign -1 = counter clockwise
		void rotateFrontSide(int sign);
		void rotateBottomSide(int sign);
		void rotateBackSide(int sign);
		void rotateLeftSide(int sign);
		void rotateRightSide(int sign);
		
		inline float getRotationAlpha() { return rotationAlpha; }
		inline void setRotationAlpha(float a) { rotationAlpha = a; }
		inline MicroCube *getMicroCubes() { return microCubes; }
};