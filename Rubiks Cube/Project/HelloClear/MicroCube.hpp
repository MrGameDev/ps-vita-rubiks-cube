#pragma once
#include <stdint.h>
#include <vectormath.h>
#include "CubeGraphics.hpp"

using namespace sce::Vectormath::Simd::Aos;


class MicroCube
{
	private:
		BasicVertex *vertexData, *copyVertexData;
		uint16_t *indexData, *copyIndexData;
		int32_t basicVerticesUId;
		int32_t	basicIndiceUId;

		uint32_t colors[6]; //current color for each side

		Matrix4 rotation;
		Vector3 center;

	public:
		MicroCube();
		MicroCube(Vector3 center);
		~MicroCube();

		void createMicroCubeData(uint32_t *colors);
		void destroyMicroCubeData();

		void updateColors(uint32_t colors);

		void renderMicroCube();

		void rotateX(Vector3 transVec, float rad);
		void rotateY(Vector3 transVec, float rad);
		void rotateZ(Vector3 transVec, float rad);

		inline Vector3 getCenter() { return center; }
};

