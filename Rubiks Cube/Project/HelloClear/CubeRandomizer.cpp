#include "CubeRandomizer.hpp"

void CubeRandomizer::randomizeCube(Cube *cube)
{
	int topRotations = m_randomizer() % MAX_ROTATIONS;
	int frontRotations = m_randomizer() % MAX_ROTATIONS;
	int bottomRotations = m_randomizer() % MAX_ROTATIONS;
	int backRotations = m_randomizer() % MAX_ROTATIONS;
	int leftRotations = m_randomizer() % MAX_ROTATIONS;
	int rightRotations = m_randomizer() % MAX_ROTATIONS;

	for (int i = 0; i < topRotations; i++)
	{
		cube->rotateTopSideClockwise();

		for (int j = 0; j < frontRotations; j++)
		{
			cube->rotateFrontSideClockwise();

			for (int k = 0; k < bottomRotations; k++)
			{
				cube->rotateBottomSideClockwise();

				for (int l = 0; l < backRotations; l++)
				{
					cube->rotateBackSideClockwise();

					for (int m = 0; m < leftRotations; m++)
					{
						cube->rotateLeftSideClockwise();

						for (int n = 0; n < rightRotations; n++)
						{
							cube->rotateRightSideClockwise();
						}
					}
				}
			}
		}
	}
}