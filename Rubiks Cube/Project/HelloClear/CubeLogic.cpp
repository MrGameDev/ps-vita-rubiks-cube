#include "CubeLogic.hpp"

/*
Initializes the cube with the same color on every side
0 = Red
1 = Green
2 = Blue
3 = Yellow
4 = White
5 = Orange
*/
CubeLogic::CubeLogic()
{
	for (uint8_t i = 0; i < this->CUBE_SIDES; i++)
	{
		//for every side of the cube
		for (uint8_t j = 0; j < this->CUBE_SIZE; j++)
		{
			//set every tile to a specific color from CubeColors
			for (uint8_t k = 0; k < this->CUBE_SIZE; k++)
			{
				this->m_wholeCube[i][j][k] = i; //color is integer value from CubeColors
			}
		}
	}
	
	for (int i = 0; i < 27; i++)
	{
		/*
			fill microcube indices
			[0 - 8] top row
			[9 - 18] center row
			[19 - 27] bottom row
		*/
		m_microCubeIndices[i] = i;
		m_copyMicroCubeIndices[i] = i;
	}

	//create index data
	uint8_t topIndices[9] = {0, 1, 2, 3, 4, 5, 6, 7, 8};
	uint8_t frontIndices[9] = {0, 1, 2, 9, 10, 11, 18, 19, 20};
	uint8_t bottomIndices[9] = {18, 19, 20, 21, 22, 23, 24, 25, 26};
	uint8_t backIndices[9] = {6, 7, 8, 15, 16, 17, 24, 25, 26};
	uint8_t leftIndices[9] = {0, 3, 6, 9, 12, 15, 18, 21, 24};
	uint8_t rightIndices[9] = {2, 5, 8, 11, 14, 17, 20, 23, 26};
	
	for (int i = 0; i < 9; i++)
	{
		m_topIndices[i] = topIndices[i];
		m_frontIndices[i] = frontIndices[i];
		m_bottomIndices[i] = bottomIndices[i];
		m_backIndices[i] = backIndices[i];
		m_leftIndices[i] = leftIndices[i];
		m_rightIndices[i] = rightIndices[i];
	}
}

CubeLogic::~CubeLogic()
{
}

/*
Copys the wholeCube into another cube
*/
void CubeLogic::updateCopyCube()
{
	//update colors
	for (uint8_t i = 0; i < this->CUBE_SIDES; i++)
	{
		for (uint8_t j = 0; j < this->CUBE_SIZE; j++)
		{
			for (uint8_t k = 0; k < this->CUBE_SIZE; k++)
			{
				m_copyCube[i][j][k] = m_wholeCube[i][j][k];
			}
		}
	}
	
	//update microCube indices
	for (int i = 0; i < 27; i++)
	{
		m_copyMicroCubeIndices[i] = m_microCubeIndices[i];
	}
}

void CubeLogic::rotateTopSideClockwise()
{
	this->updateCopyCube();
	
	/*
	top is at wholecube[side][0][i]
	*/
	for (uint8_t i = 0; i < CUBE_SIZE; i++)
	{
		//sides front, back, left and right are affected by this rotation
		m_wholeCube[front][0][i] = m_copyCube[right][0][i]; //right to front
		m_wholeCube[back][0][i] = m_copyCube[left][0][i]; //left to back
		m_wholeCube[left][0][i] = m_copyCube[front][0][i]; //front to left
		m_wholeCube[right][0][i] = m_copyCube[back][0][i]; //back to right
	}

	//rotate top tiles
	this->rotateSideTilesClockwise(top);
}

void CubeLogic::rotateTopSideCounterClockwise()
{
	this->updateCopyCube();

	for (uint8_t i = 0; i < CUBE_SIZE; i++)
	{
		m_wholeCube[front][0][i] = m_copyCube[left][0][i];
		m_wholeCube[back][0][i] = m_copyCube[right][0][i];
		m_wholeCube[left][0][i] = m_copyCube[back][0][i];
		m_wholeCube[right][0][i] = m_copyCube[front][0][i];
	}

	this->rotateSideTilesCounterClockwise(top);
}

void CubeLogic::rotateBottomSideClockwise()
{
	this->updateCopyCube();

	/*
	bottom is at wholecube[side][2][i]
	*/
	for (uint8_t i = 0; i < CUBE_SIZE; i++)
	{
		//sides front, back, left and right are affected by this rotation
		m_wholeCube[front][2][i] = m_copyCube[left][2][i];
		m_wholeCube[back][2][i] = m_copyCube[right][2][i];
		m_wholeCube[left][2][i] = m_copyCube[back][2][i];
		m_wholeCube[right][2][i] = m_copyCube[front][2][i];
	}

	//rotate bottom tiles
	this->rotateSideTilesClockwise(bottom);
}

void CubeLogic::rotateBottomSideCounterClockwise()
{
	this->updateCopyCube();

	for (uint8_t i = 0; i < CUBE_SIZE; i++)
	{
		//sides front, back, left and right are affected by this rotation
		m_wholeCube[front][2][i] = m_copyCube[right][2][i]; //right to front
		m_wholeCube[back][2][i] = m_copyCube[left][2][i]; //left to back
		m_wholeCube[left][2][i] = m_copyCube[front][2][i]; //front to left
		m_wholeCube[right][2][i] = m_copyCube[back][2][i]; //back to right
	}

	this->rotateSideTilesCounterClockwise(bottom);
}

void CubeLogic::rotateFrontSideClockwise()
{
	this->updateCopyCube();

	for (uint8_t i = 0; i < CUBE_SIZE; i++)
	{
		m_wholeCube[top][2][2 - i] = m_copyCube[left][i][2];
		m_wholeCube[right][i][0] = m_copyCube[top][2][i];
		m_wholeCube[bottom][0][2 - i] = m_copyCube[right][i][0];
		m_wholeCube[left][i][2] = m_copyCube[bottom][0][i];
	}

	this->rotateSideTilesClockwise(front);
}

void CubeLogic::rotateFrontSideCounterClockwise()
{
	this->updateCopyCube();

	for (uint8_t i = 0; i < CUBE_SIZE; i++)
	{
		m_wholeCube[top][2][i] = m_copyCube[right][i][0];
		m_wholeCube[right][2 - i][0] = m_copyCube[bottom][0][i];
		m_wholeCube[bottom][0][i] = m_copyCube[left][i][2];
		m_wholeCube[left][2 - i][2] = m_copyCube[top][2][i];
	}

	this->rotateSideTilesCounterClockwise(front);
}

void CubeLogic::rotateBackSideClockwise()
{
	this->updateCopyCube();

	for (uint8_t i = 0; i < CUBE_SIZE; i++)
	{
		m_wholeCube[top][0][i] = m_copyCube[right][i][2];
		m_wholeCube[right][2 - i][2] = m_copyCube[bottom][2][i];
		m_wholeCube[bottom][2][i] = m_copyCube[left][i][0];
		m_wholeCube[left][2 - i][0] = m_copyCube[top][0][i];
	}

	this->rotateSideTilesClockwise(back);
}

void CubeLogic::rotateBackSideCounterClockwise()
{
	this->updateCopyCube();

	for (uint8_t i = 0; i < CUBE_SIZE; i++)
	{
		m_wholeCube[top][0][2 - i] = m_copyCube[left][i][0];
		m_wholeCube[right][i][2] = m_copyCube[top][0][i];
		m_wholeCube[bottom][2][2 - i] = m_copyCube[right][i][2];
		m_wholeCube[left][i][0] = m_copyCube[bottom][2][i];
	}

	this->rotateSideTilesCounterClockwise(back);
}

void CubeLogic::rotateLeftSideClockwise()
{
	this->updateCopyCube();

	for (uint8_t i = 0; i < CUBE_SIZE; i++)
	{
		m_wholeCube[top][2 - i][0] = m_copyCube[back][i][2];
		m_wholeCube[front][i][0] = m_copyCube[top][i][0];
		m_wholeCube[bottom][i][0] = m_copyCube[front][i][0];
		m_wholeCube[back][2 - i][2] = m_copyCube[bottom][i][0];
	}

	this->rotateSideTilesClockwise(left);
}

void CubeLogic::rotateLeftSideCounterClockwise()
{
	this->updateCopyCube();

	for (uint8_t i = 0; i < CUBE_SIZE; i++)
	{
		m_wholeCube[top][i][0] = m_copyCube[front][i][0];
		m_wholeCube[front][i][0] = m_copyCube[bottom][i][0];
		m_wholeCube[bottom][i][0] = m_copyCube[back][2 - i][2];
		m_wholeCube[back][2 - i][2] = m_copyCube[top][i][0];
	}

	this->rotateSideTilesCounterClockwise(left);
}

void CubeLogic::rotateRightSideClockwise()
{
	this->updateCopyCube();

	for (uint8_t i = 0; i < CUBE_SIZE; i++)
	{
		m_wholeCube[top][i][2] = m_copyCube[front][i][2];
		m_wholeCube[front][i][2] = m_copyCube[bottom][i][2];
		m_wholeCube[bottom][i][2] = m_copyCube[back][2 - i][0];
		m_wholeCube[back][2 - i][0] = m_copyCube[top][i][2];
	}

	this->rotateSideTilesClockwise(right);
}

void CubeLogic::rotateRightSideCounterClockwise()
{
	this->updateCopyCube();

	for (uint8_t i = 0; i < CUBE_SIZE; i++)
	{
		m_wholeCube[top][i][2] = m_copyCube[back][2 - i][0];
		m_wholeCube[front][i][2] = m_copyCube[top][i][2];
		m_wholeCube[bottom][i][2] = m_copyCube[front][i][2];
		m_wholeCube[back][2 - i][0] = m_copyCube[bottom][i][2];
	}

	this->rotateSideTilesCounterClockwise(right);
}

/*
rotates a specific side clockwise
TODO: optimize switch/cases
*/
void CubeLogic::rotateSideTilesClockwise(uint8_t side)
{
	//color
	for (uint8_t i = 0; i < CUBE_SIZE; i++)
	{
		m_wholeCube[side][0][i] = m_copyCube[side][2 - i][0];
		m_wholeCube[side][1][i] = m_copyCube[side][2 - i][1];
		m_wholeCube[side][2][i] = m_copyCube[side][2 - i][2];
	}

	//micro cube indices
	switch (side)
	{
		case top: {
			uint8_t topIndices[9] = {m_topIndices[2], m_topIndices[5], m_topIndices[8], m_topIndices[1], m_topIndices[4], m_topIndices[7], m_topIndices[0], m_topIndices[3], m_topIndices[6]};

			//update other affected sides
			uint8_t leftIndices[9] = {topIndices[0], topIndices[3], topIndices[6], m_leftIndices[3], m_leftIndices[4], m_leftIndices[5], m_leftIndices[6], m_leftIndices[7], m_leftIndices[8]};
			uint8_t rightIndices[9] = {topIndices[2], topIndices[5], topIndices[8], m_rightIndices[3], m_rightIndices[4], m_rightIndices[5], m_rightIndices[6], m_rightIndices[7], m_rightIndices[8]};
			uint8_t frontIndices[9] = {topIndices[0], topIndices[1], topIndices[2], m_frontIndices[3], m_frontIndices[4], m_frontIndices[5], m_frontIndices[6], m_frontIndices[7], m_frontIndices[8]};
			uint8_t backIndices[9] = {topIndices[6], topIndices[7], topIndices[8], m_backIndices[3], m_backIndices[4], m_backIndices[5], m_backIndices[6], m_backIndices[7], m_backIndices[8]};

			for (int i = 0; i < 9; i++)
			{
				m_topIndices[i] = topIndices[i];
				m_rightIndices[i] = rightIndices[i];
				m_leftIndices[i] = leftIndices[i];
				m_frontIndices[i] = frontIndices[i];
				m_backIndices[i] = backIndices[i];
			}
			
		} break;
		case bottom: {
			uint8_t bottomIndices[9] = {m_bottomIndices[6], m_bottomIndices[3], m_bottomIndices[0], m_bottomIndices[7], m_bottomIndices[4], m_bottomIndices[1], m_bottomIndices[8], m_bottomIndices[5], m_bottomIndices[2]};

			//update other affected sides
			uint8_t leftIndices[9] = {m_leftIndices[0], m_leftIndices[1], m_leftIndices[2], m_leftIndices[3], m_leftIndices[4], m_leftIndices[5], bottomIndices[0], bottomIndices[3], bottomIndices[6]};
			uint8_t rightIndices[9] = {m_rightIndices[0], m_rightIndices[1], m_rightIndices[2], m_rightIndices[3], m_rightIndices[4], m_rightIndices[5], bottomIndices[2], bottomIndices[5], bottomIndices[8]};
			uint8_t frontIndices[9] = {m_frontIndices[0], m_frontIndices[1], m_frontIndices[2], m_frontIndices[3], m_frontIndices[4], m_frontIndices[5], bottomIndices[0], bottomIndices[1], bottomIndices[2]};
			uint8_t backIndices[9] = {m_backIndices[0], m_backIndices[1], m_backIndices[2], m_backIndices[3], m_backIndices[4], m_backIndices[5], bottomIndices[6], bottomIndices[7], bottomIndices[8]};

			for (int i = 0; i < 9; i++)
			{
				m_bottomIndices[i] = bottomIndices[i];
				m_rightIndices[i] = rightIndices[i];
				m_leftIndices[i] = leftIndices[i];
				m_frontIndices[i] = frontIndices[i];
				m_backIndices[i] = backIndices[i];
			}

		} break;
		case front: {
			uint8_t frontIndices[9] = {m_frontIndices[6], m_frontIndices[3], m_frontIndices[0], m_frontIndices[7], m_frontIndices[4], m_frontIndices[1], m_frontIndices[8], m_frontIndices[5], m_frontIndices[2]};

			//update other affected sides
			uint8_t leftIndices[9] = {frontIndices[0], m_leftIndices[1], m_leftIndices[2], frontIndices[3], m_leftIndices[4], m_leftIndices[5], frontIndices[6], m_leftIndices[7], m_leftIndices[8]};
			uint8_t rightIndices[9] = {frontIndices[2], m_rightIndices[1], m_rightIndices[2], frontIndices[5], m_rightIndices[4], m_rightIndices[5], frontIndices[8], m_rightIndices[7], m_rightIndices[8]};
			uint8_t topIndices[9] = {frontIndices[0], frontIndices[1], frontIndices[2], m_topIndices[3], m_topIndices[4], m_topIndices[5], m_topIndices[6], m_topIndices[7], m_topIndices[8]};
			uint8_t bottomIndices[9] = {frontIndices[6], frontIndices[7], frontIndices[8], m_bottomIndices[3], m_bottomIndices[4], m_bottomIndices[5], m_bottomIndices[6], m_bottomIndices[7], m_bottomIndices[8]};

			for (int i = 0; i < 9; i++)
			{
				m_frontIndices[i] = frontIndices[i];
				m_leftIndices[i] = leftIndices[i];
				m_rightIndices[i] = rightIndices[i];
				m_topIndices[i] = topIndices[i];
				m_bottomIndices[i] = bottomIndices[i];
			}

		} break;
		case back: {
			uint8_t backIndices[9] = {m_backIndices[2], m_backIndices[5], m_backIndices[8], m_backIndices[1], m_backIndices[4], m_backIndices[7], m_backIndices[0], m_backIndices[3], m_backIndices[6]};

			//update other affected sides
			uint8_t leftIndices[9] = {m_leftIndices[0], m_leftIndices[1], backIndices[0], m_leftIndices[3], m_leftIndices[4], backIndices[3], m_leftIndices[6], m_leftIndices[7], backIndices[6]};
			uint8_t rightIndices[9] = {m_rightIndices[0], m_rightIndices[1], backIndices[2], m_rightIndices[3], m_rightIndices[4], backIndices[5], m_rightIndices[6], m_rightIndices[7], backIndices[8]};
			uint8_t topIndices[9] = {m_topIndices[0], m_topIndices[1], m_topIndices[2], m_topIndices[3], m_topIndices[4], m_topIndices[5], backIndices[0], backIndices[1], backIndices[2]};
			uint8_t bottomIndices[9] = {m_bottomIndices[0], m_bottomIndices[1], m_bottomIndices[2], m_bottomIndices[3], m_bottomIndices[4], m_bottomIndices[5], backIndices[6], backIndices[7], backIndices[8]};

			for (int i = 0; i < 9; i++)
			{
				m_backIndices[i] = backIndices[i];
				m_leftIndices[i] = leftIndices[i];
				m_rightIndices[i] = rightIndices[i];
				m_topIndices[i] = topIndices[i];
				m_bottomIndices[i] = bottomIndices[i];
			}

		} break;
		case left: {
			uint8_t leftIndices[9] = {m_leftIndices[2], m_leftIndices[5], m_leftIndices[8], m_leftIndices[1], m_leftIndices[4], m_leftIndices[7], m_leftIndices[0], m_leftIndices[3], m_leftIndices[6]};

			//update other affected sides
			uint8_t topIndices[9] = {leftIndices[0], m_topIndices[1], m_topIndices[2], leftIndices[1], m_topIndices[4], m_topIndices[5], leftIndices[2], m_topIndices[7], m_topIndices[8]};
			uint8_t frontIndices[9] = {leftIndices[0], m_frontIndices[1], m_frontIndices[2], leftIndices[3], m_frontIndices[4], m_frontIndices[5], leftIndices[6], m_frontIndices[7], m_frontIndices[8]};
			uint8_t bottomIndices[9] = {leftIndices[6], m_bottomIndices[1], m_bottomIndices[2], leftIndices[7], m_bottomIndices[4], m_bottomIndices[5], leftIndices[8], m_bottomIndices[7], m_bottomIndices[8]};
			uint8_t backIndices[9] = {leftIndices[2], m_backIndices[1], m_backIndices[2], leftIndices[5], m_backIndices[4], m_backIndices[5], leftIndices[8], m_backIndices[7], m_backIndices[8]};

			for (int i = 0; i < 9; i++)
			{
				m_leftIndices[i] = leftIndices[i];
				m_topIndices[i] = topIndices[i];
				m_frontIndices[i] = frontIndices[i];
				m_bottomIndices[i] = bottomIndices[i];
				m_backIndices[i] = backIndices[i];
			}

		} break;
		case right: {
			uint8_t rightIndices[9] = {m_rightIndices[6], m_rightIndices[3], m_rightIndices[0], m_rightIndices[7], m_rightIndices[4], m_rightIndices[1], m_rightIndices[8], m_rightIndices[5], m_rightIndices[2]};

			//update other affected sides
			uint8_t topIndices[9] = {m_topIndices[0], m_topIndices[1], rightIndices[0], m_topIndices[3], m_topIndices[4], rightIndices[1], m_topIndices[6], m_topIndices[7], rightIndices[2]};
			uint8_t frontIndices[9] = {m_frontIndices[0], m_frontIndices[1], rightIndices[0], m_frontIndices[3], m_frontIndices[4], rightIndices[3], m_frontIndices[6], m_frontIndices[7], rightIndices[6]};
			uint8_t bottomIndices[9] = {m_bottomIndices[0], m_bottomIndices[1], rightIndices[6], m_bottomIndices[3], m_bottomIndices[4], rightIndices[7], m_bottomIndices[6], m_bottomIndices[7], rightIndices[8]};
			uint8_t backIndices[9] = {m_backIndices[0], m_backIndices[1], rightIndices[2], m_backIndices[3], m_backIndices[4], rightIndices[5], m_backIndices[6], m_backIndices[7], rightIndices[8]};

			for (int i = 0; i < 9; i++)
			{
				m_rightIndices[i] = rightIndices[i];
				m_topIndices[i] = topIndices[i];
				m_frontIndices[i] = frontIndices[i];
				m_bottomIndices[i] = bottomIndices[i];
				m_backIndices[i] = backIndices[i];
			}

		} break;
	}
}

/*
rotates a specific side counter-clockwise
*/
void CubeLogic::rotateSideTilesCounterClockwise(uint8_t side)
{
	for (uint8_t i = 0; i < CUBE_SIZE; i++)
	{
		m_wholeCube[side][0][i] = m_copyCube[side][i][2];
		m_wholeCube[side][1][i] = m_copyCube[side][i][1];
		m_wholeCube[side][2][i] = m_copyCube[side][i][0];
	}
	
	//micro cube indices
	switch (side)
	{
		case top: {
			uint8_t topIndices[9] = {m_topIndices[6], m_topIndices[3], m_topIndices[0], m_topIndices[7], m_topIndices[4], m_topIndices[1], m_topIndices[8], m_topIndices[5], m_topIndices[2]};

			//update other affected sides
			uint8_t leftIndices[9] = {topIndices[0], topIndices[3], topIndices[6], m_leftIndices[3], m_leftIndices[4], m_leftIndices[5], m_leftIndices[6], m_leftIndices[7], m_leftIndices[8]};
			uint8_t rightIndices[9] = {topIndices[2], topIndices[5], topIndices[8], m_rightIndices[3], m_rightIndices[4], m_rightIndices[5], m_rightIndices[6], m_rightIndices[7], m_rightIndices[8]};
			uint8_t frontIndices[9] = {topIndices[0], topIndices[1], topIndices[2], m_frontIndices[3], m_frontIndices[4], m_frontIndices[5], m_frontIndices[6], m_frontIndices[7], m_frontIndices[8]};
			uint8_t backIndices[9] = {topIndices[6], topIndices[7], topIndices[8], m_backIndices[3], m_backIndices[4], m_backIndices[5], m_backIndices[6], m_backIndices[7], m_backIndices[8]};

			for (int i = 0; i < 9; i++)
			{
				m_topIndices[i] = topIndices[i];
				m_rightIndices[i] = rightIndices[i];
				m_leftIndices[i] = leftIndices[i];
				m_frontIndices[i] = frontIndices[i];
				m_backIndices[i] = backIndices[i];
			}
			
		} break;
		case bottom: {
			uint8_t bottomIndices[9] = {m_bottomIndices[2], m_bottomIndices[5], m_bottomIndices[8], m_bottomIndices[1], m_bottomIndices[4], m_bottomIndices[7], m_bottomIndices[0], m_bottomIndices[3], m_bottomIndices[6]};

			//update other affected sides
			uint8_t leftIndices[9] = {m_leftIndices[0], m_leftIndices[1], m_leftIndices[2], m_leftIndices[3], m_leftIndices[4], m_leftIndices[5], bottomIndices[0], bottomIndices[3], bottomIndices[6]};
			uint8_t rightIndices[9] = {m_rightIndices[0], m_rightIndices[1], m_rightIndices[2], m_rightIndices[3], m_rightIndices[4], m_rightIndices[5], bottomIndices[2], bottomIndices[5], bottomIndices[8]};
			uint8_t frontIndices[9] = {m_frontIndices[0], m_frontIndices[1], m_frontIndices[2], m_frontIndices[3], m_frontIndices[4], m_frontIndices[5], bottomIndices[0], bottomIndices[1], bottomIndices[2]};
			uint8_t backIndices[9] = {m_backIndices[0], m_backIndices[1], m_backIndices[2], m_backIndices[3], m_backIndices[4], m_backIndices[5], bottomIndices[6], bottomIndices[7], bottomIndices[8]};

			for (int i = 0; i < 9; i++)
			{
				m_bottomIndices[i] = bottomIndices[i];
				m_rightIndices[i] = rightIndices[i];
				m_leftIndices[i] = leftIndices[i];
				m_frontIndices[i] = frontIndices[i];
				m_backIndices[i] = backIndices[i];
			}

		} break;
		case front: {
			uint8_t frontIndices[9] = {m_frontIndices[2], m_frontIndices[5], m_frontIndices[8], m_frontIndices[1], m_frontIndices[4], m_frontIndices[7], m_frontIndices[0], m_frontIndices[3], m_frontIndices[6]};

			//update other affected sides
			uint8_t leftIndices[9] = {frontIndices[0], m_leftIndices[1], m_leftIndices[2], frontIndices[3], m_leftIndices[4], m_leftIndices[5], frontIndices[6], m_leftIndices[7], m_leftIndices[8]};
			uint8_t rightIndices[9] = {frontIndices[2], m_rightIndices[1], m_rightIndices[2], frontIndices[5], m_rightIndices[4], m_rightIndices[5], frontIndices[8], m_rightIndices[7], m_rightIndices[8]};
			uint8_t topIndices[9] = {frontIndices[0], frontIndices[1], frontIndices[2], m_topIndices[3], m_topIndices[4], m_topIndices[5], m_topIndices[6], m_topIndices[7], m_topIndices[8]};
			uint8_t bottomIndices[9] = {frontIndices[6], frontIndices[7], frontIndices[8], m_bottomIndices[3], m_bottomIndices[4], m_bottomIndices[5], m_bottomIndices[6], m_bottomIndices[7], m_bottomIndices[8]};

			for (int i = 0; i < 9; i++)
			{
				m_frontIndices[i] = frontIndices[i];
				m_leftIndices[i] = leftIndices[i];
				m_rightIndices[i] = rightIndices[i];
				m_topIndices[i] = topIndices[i];
				m_bottomIndices[i] = bottomIndices[i];
			}

		} break;
		case back: {
			uint8_t backIndices[9] = {m_backIndices[6], m_backIndices[3], m_backIndices[0], m_backIndices[7], m_backIndices[4], m_backIndices[1], m_backIndices[8], m_backIndices[5], m_backIndices[2]};

			//update other affected sides
			uint8_t leftIndices[9] = {m_leftIndices[0], m_leftIndices[1], backIndices[0], m_leftIndices[3], m_leftIndices[4], backIndices[3], m_leftIndices[6], m_leftIndices[7], backIndices[6]};
			uint8_t rightIndices[9] = {m_rightIndices[0], m_rightIndices[1], backIndices[2], m_rightIndices[3], m_rightIndices[4], backIndices[5], m_rightIndices[6], m_rightIndices[7], backIndices[8]};
			uint8_t topIndices[9] = {m_topIndices[0], m_topIndices[1], m_topIndices[2], m_topIndices[3], m_topIndices[4], m_topIndices[5], backIndices[0], backIndices[1], backIndices[2]};
			uint8_t bottomIndices[9] = {m_bottomIndices[0], m_bottomIndices[1], m_bottomIndices[2], m_bottomIndices[3], m_bottomIndices[4], m_bottomIndices[5], backIndices[6], backIndices[7], backIndices[8]};

			for (int i = 0; i < 9; i++)
			{
				m_backIndices[i] = backIndices[i];
				m_leftIndices[i] = leftIndices[i];
				m_rightIndices[i] = rightIndices[i];
				m_topIndices[i] = topIndices[i];
				m_bottomIndices[i] = bottomIndices[i];
			}

		} break;
		case left: {
			uint8_t leftIndices[9] = {m_leftIndices[6], m_leftIndices[3], m_leftIndices[0], m_leftIndices[7], m_leftIndices[4], m_leftIndices[1], m_leftIndices[8], m_leftIndices[5], m_leftIndices[2]};

			//update other affected sides
			uint8_t topIndices[9] = {leftIndices[0], m_topIndices[1], m_topIndices[2], leftIndices[1], m_topIndices[4], m_topIndices[5], leftIndices[2], m_topIndices[7], m_topIndices[8]};
			uint8_t frontIndices[9] = {leftIndices[0], m_frontIndices[1], m_frontIndices[2], leftIndices[3], m_frontIndices[4], m_frontIndices[5], leftIndices[6], m_frontIndices[7], m_frontIndices[8]};
			uint8_t bottomIndices[9] = {leftIndices[6], m_bottomIndices[1], m_bottomIndices[2], leftIndices[7], m_bottomIndices[4], m_bottomIndices[5], leftIndices[8], m_bottomIndices[7], m_bottomIndices[8]};
			uint8_t backIndices[9] = {leftIndices[2], m_backIndices[1], m_backIndices[2], leftIndices[5], m_backIndices[4], m_backIndices[5], leftIndices[8], m_backIndices[7], m_backIndices[8]};

			for (int i = 0; i < 9; i++)
			{
				m_leftIndices[i] = leftIndices[i];
				m_topIndices[i] = topIndices[i];
				m_frontIndices[i] = frontIndices[i];
				m_bottomIndices[i] = bottomIndices[i];
				m_backIndices[i] = backIndices[i];
			}

		} break;
		case right: {
			uint8_t rightIndices[9] = {m_rightIndices[2], m_rightIndices[5], m_rightIndices[8], m_rightIndices[1], m_rightIndices[4], m_rightIndices[7], m_rightIndices[0], m_rightIndices[3], m_rightIndices[6]};

			//update other affected sides
			uint8_t topIndices[9] = {m_topIndices[0], m_topIndices[1], rightIndices[0], m_topIndices[3], m_topIndices[4], rightIndices[1], m_topIndices[6], m_topIndices[7], rightIndices[2]};
			uint8_t frontIndices[9] = {m_frontIndices[0], m_frontIndices[1], rightIndices[0], m_frontIndices[3], m_frontIndices[4], rightIndices[3], m_frontIndices[6], m_frontIndices[7], rightIndices[6]};
			uint8_t bottomIndices[9] = {m_bottomIndices[0], m_bottomIndices[1], rightIndices[6], m_bottomIndices[3], m_bottomIndices[4], rightIndices[7], m_bottomIndices[6], m_bottomIndices[7], rightIndices[8]};
			uint8_t backIndices[9] = {m_backIndices[0], m_backIndices[1], rightIndices[2], m_backIndices[3], m_backIndices[4], rightIndices[5], m_backIndices[6], m_backIndices[7], rightIndices[8]};

			for (int i = 0; i < 9; i++)
			{
				m_rightIndices[i] = rightIndices[i];
				m_topIndices[i] = topIndices[i];
				m_frontIndices[i] = frontIndices[i];
				m_bottomIndices[i] = bottomIndices[i];
				m_backIndices[i] = backIndices[i];
			}

		} break;
	}
}

uint8_t* CubeLogic::getMicroCubeIndices()
{
	return m_microCubeIndices;
}