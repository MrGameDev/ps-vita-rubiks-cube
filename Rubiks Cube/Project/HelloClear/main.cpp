#include <stdbool.h>
#include <sceerror.h>
#include <sstream>
#include <ctrl.h>
#include "InputManager.hpp"
#include "CubeRenderer.hpp"
#include "CubeHandler.hpp"

/*	@brief libc parameters */
unsigned int sceLibcHeapSize = 1*1024*1024;

/*	@brief User main thread parameters */
extern const char			sceUserMainThreadName[]		= "simple_main_thr";
extern const int			sceUserMainThreadPriority	= SCE_KERNEL_DEFAULT_PRIORITY_USER;
extern const unsigned int	sceUserMainThreadStackSize	= SCE_KERNEL_STACK_SIZE_DEFAULT_USER_MAIN;


void render();
void Update();

CubeHandler cubeHandler;

/* Main entry point of program */
int main( void )
{
	int returnCode = SCE_OK;

	//init input manager
	g_pInputManager->init();

	//init libgxm
	returnCode = g_pCubeGraphics->initGxm();
	SCE_DBG_ALWAYS_ASSERT( returnCode == SCE_OK );

	//init cubeRenderer
	g_pCubeRenderer->init();

	/* create gxm graphics data */
	g_pCubeGraphics->createGxmData();

	/* 6. main loop */
	while (true)
	{
        Update();
		render();
		g_pCubeGraphics->cycleDisplayBuffers();

		//quit when start button is pressed
		if (g_pInputManager->isStartButtonPressed())
			break;
	}
	
	// 10. wait until rendering is done 
	sceGxmFinish(g_pCubeGraphics->getContext());

	// destroy gxm graphics data 
	g_pCubeGraphics->destroyGxmData();

	// shutdown libdbgfont and libgxm 
	returnCode = g_pCubeGraphics->shutdownGxm();
	SCE_DBG_ALWAYS_ASSERT( returnCode == SCE_OK );

	return returnCode;
}

void Update ()
{
	g_pInputManager->update();
    g_pCubeGraphics->updateGraphics();
	cubeHandler.update();
}

void render()
{
	g_pCubeGraphics->renderGxm();

	g_pCubeRenderer->renderWholeCube();

	/* stop rendering to the render target */
	sceGxmEndScene( g_pCubeGraphics->context, NULL, NULL );
}