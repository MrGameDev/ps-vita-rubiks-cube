#pragma once

#include <stdint.h>
#include "Singleton.hpp"

#define g_pCubeLogic CubeLogic::Get()

class CubeLogic : public Singleton<CubeLogic>
{
	private:
		enum CubeColors { red, green, blue, yellow, white, orange };
		enum CubeSides { top, front, bottom, back, left, right };

		static const short CUBE_SIDES = 6, CUBE_SIZE = 3;
	
		//colors
		uint8_t m_wholeCube[CUBE_SIDES][CUBE_SIZE][CUBE_SIZE]; //1st dim = side of cube, 2nd / 3rd dim = color of tile
		uint8_t m_copyCube[CUBE_SIDES][CUBE_SIZE][CUBE_SIZE];
		
		//microCube indices
		uint8_t m_microCubeIndices[27];
		uint8_t m_copyMicroCubeIndices[27];

	public:
		CubeLogic();
		~CubeLogic();
		
		uint8_t m_topIndices[9], m_frontIndices[9], m_bottomIndices[9], m_backIndices[9], m_leftIndices[9], m_rightIndices[9];

		void rotateTopSideClockwise();
		void rotateTopSideCounterClockwise();
		void rotateFrontSideClockwise();
		void rotateFrontSideCounterClockwise();
		void rotateBottomSideClockwise();
		void rotateBottomSideCounterClockwise();
		void rotateBackSideClockwise();
		void rotateBackSideCounterClockwise();
		void rotateLeftSideClockwise();
		void rotateLeftSideCounterClockwise();
		void rotateRightSideClockwise();
		void rotateRightSideCounterClockwise();

		void rotateSideTilesClockwise(uint8_t side);
		void rotateSideTilesCounterClockwise(uint8_t side);

		void updateCopyCube();
		
		uint8_t* getMicroCubeIndices();
};