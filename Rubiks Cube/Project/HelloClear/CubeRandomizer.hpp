#pragma once

#include "Cube.hpp"
#include <random>

class CubeRandomizer
{
	private:
		const int MAX_ROTATIONS = 3;
		std::random_device m_randomizer;

	public:
		void randomizeCube(Cube *cube);
};