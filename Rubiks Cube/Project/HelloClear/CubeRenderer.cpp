#include "CubeRenderer.hpp"
#include <cmath>

/*
 microCubes[0 - 8] = top row
 microCubes[9 - 18] = center row
 microCubes[19 - 27] = bottom row
*/
CubeRenderer::CubeRenderer()
{
	this->tileSize = 0.5f;
	
	rotationAlpha = 0.f;
	rotationSpeed = 2.f;
}

CubeRenderer::~CubeRenderer()
{
	for (int i = 0; i < 27; i++)
	{
		microCubes[i].destroyMicroCubeData();
	}
}

void CubeRenderer::init()
{
	createWholeCube();
}

void CubeRenderer::createWholeCube()
{
	int centerX = 0, centerY = -4, centerZ = -4; //translate, so that the cube is centered

	for (int i = 0; i < 27; i++)
	{
		//update center
		centerX = ((i % 3) * 2) - 2; //0 = first xRow, 2 = 2nd, 4 = 3rd

		if (i % 9 == 0) //0 = first yRow, 2 = 2nd, 4 = 3rd
		{
			centerY += 2;
			centerZ = -4; //reset z position
		}

		if (i % 3 == 0) //0 = first zRow, 2 = second zRow, 4 = third zRow
			centerZ += 2;

		//left front -> right front <- left center -> right center <- left back -> right back
		microCubes[i] = MicroCube(Vector3(centerX, centerY, centerZ));

		//uint32_t colors[6] = {0x065874ff, 0x4799abff, 0x547821ff, 0x852147ff, 0x365214ff, 0x917382ff};
		uint32_t colors[6] = {0xabcdefff, 0xfedcbaff, 0xcbadefff, 0x5648cdff, 0x845756ff, 0x154684ff};
		
		microCubes[i].createMicroCubeData(colors);
	}
}

void CubeRenderer::renderWholeCube()
{
	for (int i = 0; i < 27; i++)
	{
		microCubes[i].renderMicroCube();
	}
}

//--- ROTATION ---//

void CubeRenderer::rotateTopSide(int sign)
{
	for (int i = 0; i < 9; i++)
	{
		microCubes[g_pCubeLogic->m_topIndices[i]].rotateY(microCubes[4].getCenter(), sign * rotationSpeed * RAD);
	}
		
	rotationAlpha += rotationSpeed;
}

void CubeRenderer::rotateFrontSide(int sign)
{
	for (int i = 0; i < 9; i++)
	{
		microCubes[g_pCubeLogic->m_frontIndices[i]].rotateZ(microCubes[10].getCenter(), sign * rotationSpeed * RAD);
	}
		
	rotationAlpha += rotationSpeed;
}

void CubeRenderer::rotateBottomSide(int sign)
{
	for (int i = 0; i < 9; i++)
	{
		microCubes[g_pCubeLogic->m_bottomIndices[i]].rotateY(microCubes[22].getCenter(), sign * rotationSpeed * RAD);
	}
		
	rotationAlpha += rotationSpeed;
}

void CubeRenderer::rotateBackSide(int sign)
{
	for (int i = 0; i < 9; i++)
	{
		microCubes[g_pCubeLogic->m_backIndices[i]].rotateZ(microCubes[16].getCenter(), sign * rotationSpeed * RAD);
	}
		
	rotationAlpha += rotationSpeed;
}

void CubeRenderer::rotateLeftSide(int sign)
{
	for (int i = 0; i < 9; i++)
	{
		microCubes[g_pCubeLogic->m_leftIndices[i]].rotateX(microCubes[12].getCenter(), sign * rotationSpeed * RAD);
	}
		
	rotationAlpha += rotationSpeed;
}

void CubeRenderer::rotateRightSide(int sign)
{
	for (int i = 0; i < 9; i++)
	{
		microCubes[g_pCubeLogic->m_rightIndices[i]].rotateX(microCubes[14].getCenter(), sign * rotationSpeed * RAD);
	}
		
	rotationAlpha += rotationSpeed;
}