#include "InputManager.hpp"

InputManager::InputManager()
{
}


InputManager::~InputManager()
{
}

void InputManager::init()
{
	oldTouchPositionBack = Vector2(0, 0);
	oldTouchPositionFront = Vector2(0, 0);

	//enable controls
	sceCtrlSetSamplingMode(SCE_CTRL_MODE_DIGITALANALOG_WIDE);

	//enable touch front
	sceTouchSetSamplingState(SCE_TOUCH_PORT_FRONT, SCE_TOUCH_SAMPLING_STATE_START);

	//enable touch back
	sceTouchSetSamplingState(SCE_TOUCH_PORT_BACK, SCE_TOUCH_SAMPLING_STATE_START);

	//get panel infos
	sceTouchGetPanelInfo(SCE_TOUCH_PORT_FRONT, &piFront);
	sceTouchGetPanelInfo(SCE_TOUCH_PORT_BACK, &piBack);
}

void InputManager::update()
{
	//read ctrl input
	sceCtrlPeekBufferPositive(0, &ctrlData, 1);

	//save oldpos back
	if (tdBack.reportNum > 0)
		oldTouchPositionBack = Vector2(tdBack.report[0].x, tdBack.report[0].y);

	//save oldpos front
	if (tdFront.reportNum > 0)
		oldTouchPositionFront = Vector2(tdFront.report[0].x, tdFront.report[0].y);

	//read touch input
	sceTouchRead(SCE_TOUCH_PORT_FRONT, &tdFront, 1);
	sceTouchRead(SCE_TOUCH_PORT_BACK, &tdBack, 1);
}

SceCtrlData InputManager::getData()
{
	return ctrlData;
}

Vector2 InputManager::getBackPanelTouchPositionDelta()
{
	if (tdBack.reportNum > 0)
	{
		float deltaX = tdBack.report[0].x - oldTouchPositionBack.getX();
		float deltaY = tdBack.report[0].y - oldTouchPositionBack.getY();

		float buffer = 100.f;

		if ((deltaX < buffer && deltaY < buffer && deltaX > 0 && deltaY > 0) || //positive range (only rotate if delta is in range [0, buffer])
			(deltaX > -buffer && deltaY > -buffer && deltaX < 0 && deltaY < 0)) //negative range (only rotate if delta is in range [0, -buffer])
		{
			//only rotate one axis at once
			if (abs(deltaX) > abs(deltaY))
				return Vector2(deltaX, 0.f);
			else if (abs(deltaX) < abs(deltaY))
				return Vector2(0.f, deltaY);
		}
	}
	
	return Vector2(0, 0);
}

Vector2 InputManager::getFrontPanelTouchPositionDelta()
{
	if (tdFront.reportNum > 0)
	{
		float deltaX = tdFront.report[0].x - oldTouchPositionFront.getX();
		float deltaY = tdFront.report[0].y - oldTouchPositionFront.getY();

		float buffer = 20;

		if ((deltaX < buffer && deltaY < buffer && deltaX > 0 && deltaY > 0) || //positive range (only rotate if delta is in range [0, 20])
			(deltaX > -buffer && deltaY > -buffer && deltaX < 0 && deltaY < 0)) //negative range (only rotate if delta is in range [0, -20])
			return Vector2(deltaX, deltaY);
	}

	return Vector2(0, 0);
}

Vector2 InputManager::getFrontPanelTouchPosition()
{
	if (tdFront.reportNum > 0)
		return Vector2(tdFront.report[0].x, tdFront.report[0].y);
	
	return Vector2(-1, -1);
}

SceTouchPanelInfo InputManager::getPanelInfoBack()
{
	return piBack;
}

SceTouchPanelInfo InputManager::getPanelInfoFront()
{
	return piFront;
}

//touchPos to -1 / 1 coordinates
Vector2 InputManager::touchPositionToRenderPosition(Vector2 touchPosition)
{
	float x = (float)touchPosition.getX();
	float y = (float)touchPosition.getY();

	x = (x * 2.f) / piFront.maxDispX - 1.f;
	y = (y * 2.f) / piFront.maxDispY - 1.f;

	return Vector2(x, y);
}

bool InputManager::isLButtonPressed()
{
	return (ctrlData.buttons & SCE_CTRL_L) != 0;
}

bool InputManager::isRButtonPressed()
{
	return (ctrlData.buttons & SCE_CTRL_R) != 0;
}

bool InputManager::isUpButtonPressed()
{
	return (ctrlData.buttons & SCE_CTRL_UP) != 0;
}

bool InputManager::isDownButtonPressed()
{
	return (ctrlData.buttons & SCE_CTRL_DOWN) != 0;
}

bool InputManager::isLeftButtonPressed()
{
	return (ctrlData.buttons & SCE_CTRL_LEFT) != 0;
}

bool InputManager::isRightButtonPressed()
{
	return (ctrlData.buttons & SCE_CTRL_RIGHT) != 0;
}

bool InputManager::isTriangleButtonPressed()
{
	return (ctrlData.buttons & SCE_CTRL_TRIANGLE) != 0;
}

bool InputManager::isCrossButtonPressed()
{
	return (ctrlData.buttons & SCE_CTRL_CROSS) != 0;
}

bool InputManager::isStartButtonPressed()
{
	return (ctrlData.buttons & SCE_CTRL_START) != 0;
}