#include "MicroCube.hpp"

/*
 vertices[0] = top left front
 vertices[1] = top right front
 vertices[2] = bottom left front
 vertices[3] = bottom right front
 vertices[4] = top left back
 vertices[5] = top right back
 vertices[6] = bottom left back
 vertices[7] = bottom right back
*/

MicroCube::MicroCube()
{
}

/*MicroCube::MicroCube(Vector3 xyzFactors)
{
	this->xyzFactors = xyzFactors;
}*/

MicroCube::MicroCube(Vector3 center)
{
	this->center = center;
}

MicroCube::~MicroCube()
{
}

void MicroCube::createMicroCubeData(uint32_t *colors)
{
	/* create vertex/index data */
	vertexData = (BasicVertex*)g_pCubeGraphics->graphicsAlloc( SCE_KERNEL_MEMBLOCK_TYPE_USER_RWDATA_UNCACHE, 24 * sizeof(BasicVertex), 4, SCE_GXM_MEMORY_ATTRIB_READ, &basicVerticesUId );
	indexData = (uint16_t*)g_pCubeGraphics->graphicsAlloc( SCE_KERNEL_MEMBLOCK_TYPE_USER_RWDATA_UNCACHE, 36 * sizeof(uint16_t), 2, SCE_GXM_MEMORY_ATTRIB_READ, &basicIndiceUId );
	copyVertexData = vertexData;
	copyIndexData = indexData;

	//create rotation matrix
	rotation = Matrix4::identity();

	for (int face = 0; face < 6; ++face) 
	{
		uint32_t faceColor = colors[face];

		float sign = ((face & 0x1) ? 1.0f : -1.0f);
		uint32_t axis = face >> 1;

		float ox = (axis == 0) ? sign : 0.0f;
		float oy = (axis == 1) ? sign : 0.0f;
		float oz = (axis == 2) ? sign : 0.0f;

		float ux = (axis != 0) ? 1.0f : 0.0f;
		float uy = (axis == 0) ? 1.0f : 0.0f;
		float uz = 0.0f;

		float vx = 0.0f;
		float vy = (axis == 2) ? 1.0f : 0.0f;
		float vz = (axis != 2) ? 1.0f : 0.0f;

		//left, right, top, bottom, front, back
		float normal[3] = {ox, oy, oz};

		copyVertexData->x = ox - ux - vx + center.getX();
		copyVertexData->y = oy - uy - vy + center.getY();
		copyVertexData->z = oz - uz - vz + center.getZ();
		copyVertexData->color = faceColor;
		copyVertexData->normal[0] = normal[0];
		copyVertexData->normal[1] = normal[1];
		copyVertexData->normal[2] = normal[2];
		++copyVertexData;

		copyVertexData->x = ox + ux - vx + center.getX();
		copyVertexData->y = oy + uy - vy + center.getY();
		copyVertexData->z = oz + uz - vz + center.getZ();
		copyVertexData->color = faceColor;
		copyVertexData->normal[0] = normal[0];
		copyVertexData->normal[1] = normal[1];
		copyVertexData->normal[2] = normal[2];
		++copyVertexData;

		copyVertexData->x = ox + ux + vx + center.getX();
		copyVertexData->y = oy + uy + vy + center.getY();
		copyVertexData->z = oz + uz + vz + center.getZ();
		copyVertexData->color = faceColor;
		copyVertexData->normal[0] = normal[0];
		copyVertexData->normal[1] = normal[1];
		copyVertexData->normal[2] = normal[2];
		++copyVertexData;

		copyVertexData->x = ox - ux + vx + center.getX();
		copyVertexData->y = oy - uy + vy + center.getY();
		copyVertexData->z = oz - uz + vz + center.getZ();
		copyVertexData->color = faceColor;
		copyVertexData->normal[0] = normal[0];
		copyVertexData->normal[1] = normal[1];
		copyVertexData->normal[2] = normal[2];
		++copyVertexData;
	}

	// write indices
	for (uint32_t face = 0; face < 6; ++face) 
	{
		uint32_t offset = 4*face;

		*copyIndexData++ = offset + 0;
		*copyIndexData++ = offset + 1;
		*copyIndexData++ = offset + 2;

		*copyIndexData++ = offset + 2;
		*copyIndexData++ = offset + 3;
		*copyIndexData++ = offset + 0;
	}
}

void MicroCube::destroyMicroCubeData()
{
	g_pCubeGraphics->graphicsFree(basicVerticesUId);
	g_pCubeGraphics->graphicsFree(basicIndiceUId);
}

void MicroCube::renderMicroCube()
{
	// draw the cube
	sceGxmSetVertexStream( g_pCubeGraphics->context, 0, vertexData );
	sceGxmDraw( g_pCubeGraphics->context, SCE_GXM_PRIMITIVE_TRIANGLES, SCE_GXM_INDEX_FORMAT_U16, indexData, 36 );
}

//rotates vertices and normals
void MicroCube::rotateX(Vector3 transVec, float rad)
{ 
	Matrix4 trans = Matrix4::translation(transVec); //translate to center
	Matrix4 backTrans = Matrix4::translation(-transVec);
	rotation = Matrix4::rotationX(rad);
	Vector4 currentVertex, rotatedVertex;
	Vector3 currentNormal, rotatedNormal;

	for (int i = 0; i < 24; i++)
	{
		currentVertex = Vector4(vertexData[i].x, vertexData[i].y, vertexData[i].z, 1.f);
		rotatedVertex = trans * rotation * backTrans * currentVertex;

		currentNormal = Vector3(vertexData[i].normal[0], vertexData[i].normal[1], vertexData[i].normal[2]);
		rotatedNormal = rotation.getUpper3x3() * currentNormal;
		rotatedNormal = normalize(rotatedNormal);

		vertexData[i].x = rotatedVertex.getX();
		vertexData[i].y = rotatedVertex.getY();
		vertexData[i].z = rotatedVertex.getZ();

		vertexData[i].normal[0] = rotatedNormal.getX();
		vertexData[i].normal[1] = rotatedNormal.getY();
		vertexData[i].normal[2] = rotatedNormal.getZ();
	}
}

void MicroCube::rotateY(Vector3 transVec, float rad)
{ 
	Matrix4 trans = Matrix4::translation(transVec); //translate to center
	Matrix4 backTrans = Matrix4::translation(-transVec);
	rotation = Matrix4::rotationY(rad);
	Vector4 currentVertex, rotatedVertex;
	Vector3 currentNormal, rotatedNormal;

	for (int i = 0; i < 24; i++)
	{
		currentVertex = Vector4(vertexData[i].x, vertexData[i].y, vertexData[i].z, 1.f);
		rotatedVertex = trans * rotation * backTrans * currentVertex;

		currentNormal = Vector3(vertexData[i].normal[0], vertexData[i].normal[1], vertexData[i].normal[2]);
		rotatedNormal = rotation.getUpper3x3() * currentNormal;
		rotatedNormal = normalize(rotatedNormal);

		vertexData[i].x = rotatedVertex.getX();
		vertexData[i].y = rotatedVertex.getY();
		vertexData[i].z = rotatedVertex.getZ();

		vertexData[i].normal[0] = rotatedNormal.getX();
		vertexData[i].normal[1] = rotatedNormal.getY();
		vertexData[i].normal[2] = rotatedNormal.getZ();
	}
}

void MicroCube::rotateZ(Vector3 transVec, float rad)
{ 
	Matrix4 trans = Matrix4::translation(transVec); //translate to center
	Matrix4 backTrans = Matrix4::translation(-transVec);
	rotation = Matrix4::rotationZ(rad);
	Vector4 currentVertex, rotatedVertex;
	Vector3 currentNormal, rotatedNormal;

	for (int i = 0; i < 24; i++)
	{
		currentVertex = Vector4(vertexData[i].x, vertexData[i].y, vertexData[i].z, 1.f);
		rotatedVertex = trans * rotation * backTrans * currentVertex;

		currentNormal = Vector3(vertexData[i].normal[0], vertexData[i].normal[1], vertexData[i].normal[2]);
		rotatedNormal = rotation.getUpper3x3() * currentNormal;
		rotatedNormal = normalize(rotatedNormal);

		vertexData[i].x = rotatedVertex.getX();
		vertexData[i].y = rotatedVertex.getY();
		vertexData[i].z = rotatedVertex.getZ();

		vertexData[i].normal[0] = rotatedNormal.getX();
		vertexData[i].normal[1] = rotatedNormal.getY();
		vertexData[i].normal[2] = rotatedNormal.getZ();
	}
}