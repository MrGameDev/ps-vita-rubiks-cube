#pragma once
#include <kernel.h>
#include <gxm.h>
#include <libdbg.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <display.h>
#include <vectormath.h>
#include "InputManager.hpp"
#include "Singleton.hpp"

using namespace sce::Vectormath::Simd::Aos;

//DEFINES

/*	Define the width and height to render at the native resolution */
#define DISPLAY_WIDTH				960
#define DISPLAY_HEIGHT				544
#define DISPLAY_STRIDE_IN_PIXELS	1024

/*	Define the number of back buffers to use with this sample.  Most applications
	should use a value of 2 (double buffering) or 3 (triple buffering).
*/
#define DISPLAY_BUFFER_COUNT		3

/*	Define the maximum number of queued swaps that the display queue will allow.
	This limits the number of frames that the CPU can get ahead of the GPU,
	and is independent of the actual number of back buffers.  The display
	queue will block during sceGxmDisplayQueueAddEntry if this number of swaps
	have already been queued.
*/
#define DISPLAY_MAX_PENDING_SWAPS	2

#define ALIGN(x, a)					(((x) + ((a) - 1)) & ~((a) - 1))

/*	Define the libgxm color format to render to.
	This should be kept in sync with the display format to use with the SceDisplay library.
*/
#define DISPLAY_COLOR_FORMAT		SCE_GXM_COLOR_FORMAT_A8B8G8R8
#define DISPLAY_PIXEL_FORMAT		SCE_DISPLAY_PIXELFORMAT_A8B8G8R8

/*	Define the debug font pixel color format to render to. */
#define DBGFONT_PIXEL_FORMAT		SCE_DBGFONT_PIXELFORMAT_A8B8G8R8

#define g_pCubeGraphics CubeGraphics::Get()

/*	The build process for the sample embeds the shader programs directly into the
	executable using the symbols below.  This is purely for convenience, it is
	equivalent to simply load the binary file into memory and cast the contents
	to type SceGxmProgram.
*/
extern const SceGxmProgram binaryClearVGxpStart;
extern const SceGxmProgram binaryClearFGxpStart;

// !! Data related to rendering vertex.
extern const SceGxmProgram binaryBasicVGxpStart;
extern const SceGxmProgram binaryBasicFGxpStart;


//STRUCTS

/*	Data structure to pass through the display queue.  This structure is
	serialized during sceGxmDisplayQueueAddEntry, and is used to pass
	arbitrary data to the display callback function, called from an internal
	thread once the back buffer is ready to be displayed.

	In this example, we only need to pass the base address of the buffer.
*/
typedef struct DisplayData
{
	void *address;
} DisplayData;

/*	Data structure for clear geometry */
typedef struct ClearVertex
{
	float x;
	float y;
} ClearVertex;

/*	Data structure for basic geometry */
typedef struct BasicVertex
{
	float x, y, z; //position
	uint32_t color; // Data gets expanded to float4 in vertex shader.
	float normal[3];
} BasicVertex;

class CubeGraphics : public Singleton<CubeGraphics>
{
public:
	CubeGraphics();

	//VARIABLES

	SceUID vdmRingBufferUId;
	SceUID vertexRingBufferUId;
	SceUID fragmentRingBufferUId;
	SceUID fragmentUsseRingBufferUId;
	SceUID patcherFragmentUsseUId;
	SceUID patcherVertexUsseUId;
	SceUID patcherBufferUId;
	SceUID depthBufferUId;

	SceGxmContextParams contextParams; /* libgxm context parameter */
	SceGxmRenderTargetParams renderTargetParams; /* libgxm render target parameter */
	SceGxmContext *context;	/* libgxm context */
	SceGxmRenderTarget *renderTarget; /* libgxm render target */
	SceGxmShaderPatcher *shaderPatcher; /* libgxm shader patcher */

	/*	display data */
	void *displayBufferData[ DISPLAY_BUFFER_COUNT ];
	SceGxmSyncObject *displayBufferSync[ DISPLAY_BUFFER_COUNT ];
	int32_t displayBufferUId[ DISPLAY_BUFFER_COUNT ];
	SceGxmColorSurface displaySurface[ DISPLAY_BUFFER_COUNT ];
	uint32_t displayFrontBufferIndex;
	uint32_t displayBackBufferIndex;
	SceGxmDepthStencilSurface depthSurface;

	/*	shader data */
	int32_t clearVerticesUId;
	int32_t clearIndicesUId;
	SceGxmShaderPatcherId clearVertexProgramId;
	SceGxmShaderPatcherId clearFragmentProgramId;
	
	// !! Shader patcher addded.
	SceGxmShaderPatcherId basicVertexProgramId;
	SceGxmShaderPatcherId basicFragmentProgramId;

	ClearVertex *clearVertices;
	uint16_t *clearIndices;
	SceGxmVertexProgram *clearVertexProgram;
	SceGxmFragmentProgram *clearFragmentProgram;
	
	// !! Data added.
	SceGxmVertexProgram *basicVertexProgram;
	SceGxmFragmentProgram *basicFragmentProgram;

	float accumulatedTurningValueX;
	float accumulatedTurningValueY;

	Quat m_orientationQuaternion;


	//!! The program parameter for the transformation of the triangle
	float wvpData[16];
	float rotData[16];
	const SceGxmProgramParameter *wvpParam;
	const SceGxmProgramParameter *rotParam;

	Matrix4 mvpMatrix;
	Matrix4 rotMatrix;

	//FUNCTIONS

	int initGxm();
	void* graphicsAlloc(SceKernelMemBlockType type, uint32_t size, uint32_t alignment, uint32_t attribs, SceUID *uid);
	void graphicsFree( SceUID uid );

	/*	Callback function for displaying a buffer */
	static void displayCallback( const void *callbackData );

	/* Helper function to allocate memory and map it as fragment USSE code for the GPU */
	void *fragmentUsseAlloc( uint32_t size, SceUID *uid, uint32_t *usseOffset );

	/* Helper function to allocate memory and map it as vertex USSE code for the GPU */
	void *vertexUsseAlloc( uint32_t size, SceUID *uid, uint32_t *usseOffset );

	/* Callback function to allocate memory for the shader patcher */
	static void* patcherHostAlloc( void *userData, unsigned int size );

	/* Callback function to allocate memory for the shader patcher */
	static void patcherHostFree( void *userData, void *mem );

	void cycleDisplayBuffers();
	void destroyGxmData();
	int shutdownGxm();
	void vertexUsseFree( SceUID uid );
	void fragmentUsseFree( SceUID uid );

	void updateGraphics();

	void createGxmData();
	void renderGxm();

	SceGxmContext *getContext() { return context; }
};